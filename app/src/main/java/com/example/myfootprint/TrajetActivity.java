package com.example.myfootprint;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;
import java.util.Vector;

public class TrajetActivity extends AppCompatActivity {

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trajet);

        Intent message = getIntent();
        String nomTrajetChoisi = message.getStringExtra("messageVersTrajetActivity");

        /*On discrimine le type de message reçu:
        --Si le message est vide, on vient depuis CenterActivity, on part sur un état initial
        où il faut remplir la date puis les km parcourus
        --Si le message contient un String non nul (supposé avec plusieurs kilométrages)
        on vient depuis entreeTrajetActivity, et donc on souhaite ajouter les kilomètres du trajet
        préenregistreé choisi aux kilomètres déjà remplis pour la date choisie
         */
        if (nomTrajetChoisi.isEmpty()) {
            //Cas
            TextView tvMarcheVelo = findViewById(R.id.textViewMarcheVelo);
            TextView tvBus = findViewById(R.id.textViewBus);
            TextView tvVoiture = findViewById(R.id.textViewVoiture);
            TextView tvTrainRERTram = findViewById(R.id.textViewTrainRERTram);
            TextView tvAvion = findViewById(R.id.textViewAvion);
            TextView tvConsigne = findViewById(R.id.textViewConsigne);

            EditText etMarcheVelo = findViewById(R.id.editTextMarcheVelo);
            EditText etBus = findViewById(R.id.editTextBus);
            EditText etVoiture = findViewById(R.id.editTextVoiture);
            EditText etTrainRERTram = findViewById(R.id.editTextTrainRERTram);
            EditText etAvion = findViewById(R.id.editTextAvion);
            EditText etDate = findViewById(R.id.editTextDate);

            Button bEnregistrer = findViewById(R.id.buttonEnregistrer);
            Button bMesTrajets = findViewById(R.id.buttonMesTrajets);

            String dateTel = CenterActivity.getDate();
            etDate.setText(dateTel);

            tvMarcheVelo.setVisibility(TextView.INVISIBLE);
            tvBus.setVisibility(TextView.INVISIBLE);
            tvVoiture.setVisibility(TextView.INVISIBLE);
            tvTrainRERTram.setVisibility(TextView.INVISIBLE);
            tvAvion.setVisibility(TextView.INVISIBLE);
            tvConsigne.setVisibility(TextView.INVISIBLE);
            etMarcheVelo.setVisibility(EditText.INVISIBLE);
            etBus.setVisibility(EditText.INVISIBLE);
            etVoiture.setVisibility(EditText.INVISIBLE);
            etTrainRERTram.setVisibility(EditText.INVISIBLE);
            etAvion.setVisibility(EditText.INVISIBLE);
            bEnregistrer.setVisibility(EditText.INVISIBLE);
            bMesTrajets.setVisibility(EditText.INVISIBLE);
        }
        else {
            EditText etMarcheVelo = findViewById(R.id.editTextMarcheVelo);
            EditText etBus = findViewById(R.id.editTextBus);
            EditText etVoiture = findViewById(R.id.editTextVoiture);
            EditText etTrainRERTram = findViewById(R.id.editTextTrainRERTram);
            EditText etAvion = findViewById(R.id.editTextAvion);
            EditText etDate = findViewById(R.id.editTextDate);

            SharedPreferences sp = getSharedPreferences("historique", Context.MODE_PRIVATE);
            String strDateAvant = sp.getString("dernièreDate", "00/00/0000");
            String strKmAvant = sp.getString(strDateAvant, "0 0 0 0 0");

            Float[] kmAvant = CenterActivity.decode(strKmAvant);

            SharedPreferences mesTrajets = getSharedPreferences("TrajetsPreenregistres", Context.MODE_PRIVATE);
            String trajetChoisi = mesTrajets.getString(nomTrajetChoisi, "");

            Float[] kmTrajet = CenterActivity.decode(trajetChoisi);
            Float[] nouvKm = new Float[kmTrajet.length];
            for (int i = 0; i < nouvKm.length; i++) {
                nouvKm[i] = kmAvant[i] + kmTrajet[i];
            }

            Float kmMarcheVelo = nouvKm[0];
            Float kmBus = nouvKm[1];
            Float kmVoiture = nouvKm[2];
            Float kmTrainRERTram = nouvKm[3];
            Float kmAvion = nouvKm[4];

            String strMarcheVelo = Float.toString(kmMarcheVelo);
            String strBus = Float.toString(kmBus);
            String strVoiture = Float.toString(kmVoiture);
            String strTrainRERTram = Float.toString(kmTrainRERTram);
            String strAvion = Float.toString(kmAvion);

            etMarcheVelo.setText(strMarcheVelo);
            etBus.setText(strBus);
            etVoiture.setText(strVoiture);
            etTrainRERTram.setText(strTrainRERTram);
            etAvion.setText(strAvion);
            etDate.setText(strDateAvant);
        }
    }

    public void enregistrerTrajet(View view) {
        EditText etMarcheVelo = findViewById(R.id.editTextMarcheVelo);
        EditText etBus = findViewById(R.id.editTextBus);
        EditText etVoiture = findViewById(R.id.editTextVoiture);
        EditText etTrainRERTram = findViewById(R.id.editTextTrainRERTram);
        EditText etAvion = findViewById(R.id.editTextAvion);
        EditText etDate = findViewById(R.id.editTextDate);
        SharedPreferences sp = getSharedPreferences("historique", Context.MODE_PRIVATE);

        float kmMarcheVelo;
        float kmBus;
        float kmVoiture;
        float kmTrainRERTram;
        float kmAvion;

        String date = etDate.getText().toString();

        if (verifDate(date)){
            //*tt ce qui suit*
            if (etMarcheVelo.getText().toString().isEmpty()) {
                kmMarcheVelo = 0F;
            }
            else {kmMarcheVelo = Float.parseFloat(etMarcheVelo.getText().toString());}
            if (etBus.getText().toString().isEmpty()) {
                kmBus = 0F;
            }
            else {kmBus = Float.parseFloat(etBus.getText().toString());}
            if (etVoiture.getText().toString().isEmpty()) {
                kmVoiture = 0F;
            }
            else {kmVoiture = Float.parseFloat(etVoiture.getText().toString());}
            if (etTrainRERTram.getText().toString().isEmpty()) {
                kmTrainRERTram = 0F;
            }
            else {kmTrainRERTram = Float.parseFloat(etTrainRERTram.getText().toString());}
            if (etAvion.getText().toString().isEmpty()) {
                kmAvion = 0F;
            }
            else {kmAvion = Float.parseFloat(etAvion.getText().toString());}


            SharedPreferences.Editor editor = sp.edit();

            String info = Float.toString(kmMarcheVelo)+" "+Float.toString(kmBus)+" "+Float.toString(kmVoiture)+" "+Float.toString(kmTrainRERTram)+" "+Float.toString(kmAvion);
            editor.putString(date,info);
            editor.putString("dernièreDate",date);

            //insertion de "liste" 7derniers dans historique pr referencer les 7 derniers jours ou il y a eu des entrees
            //on regarde si la liste des 7 derniers jours est initialisé
            String dernStr="00/00/0000 00/00/0000 00/00/0000 00/00/0000 00/00/0000 00/00/0000 00/00/0000";
            String chaine=sp.getString("7derniers",dernStr);
            String[] dern= CenterActivity.Dates7der(chaine);
            //on trie tt ca, comme ca la plus ancienne date est en bout de liste
            CenterActivity.triDates(dern);
            //mntnt on a la chaine qui etait enregistree et on va decaler les valeurs avant d ajouter
            if (sp.contains("7derniers")){
                List<String> list = Arrays.asList(dern);
                if (list.contains(date)){
                    //fait rien, laisse comme ca, veut juste dire aue le gars rajoute des km pr ce jour
                }
                else{
                    //la date ajoutee existait pas encore: il faut regarder si elle est plus ou moins recente que la derniere des 7 dernieres enterees
                    //on commence par convertire les 2 dates en enters, comme ds la fonction trie dates
                    String ancDateStr=dern[dern.length-1];
                    String[] listechar = ancDateStr.split("/");
                    ancDateStr=listechar[2]+listechar[1]+listechar[0];
                    Integer ancDateInt= Integer.parseInt(ancDateStr);

                    String[] listechar2 = date.split("/");
                    String nouvDateStr=listechar2[2]+listechar2[1]+listechar2[0];
                    Integer nouvDateInt= Integer.parseInt(nouvDateStr);

                    if (nouvDateInt>ancDateInt){
                        //alors la nouvelle date doit etre mise a la place de la derniere de 7derniers
                        dern[dern.length-1]=date;
                        //et on remet ds l ordre
                        CenterActivity.triDates(dern);
                    }
                    else{
                        //elle est plus vieille que la plus vieille de 7derniers, on ne fait rien
                    }


                }
            }
            else{
                //alors il n'a a pas 7derniers, donc dern a pris la valeur par defaut et on peu ajouter au debut
                dern[0]=date;
            }
            dernStr=dern[0]+" "+dern[1]+" "+dern[2]+" "+dern[3]+" "+dern[4]+" "+dern[5]+" "+dern[6];
            editor.putString("7derniers",dernStr);
            //fin insertion 7derniers

            editor.apply();

            Intent versCenterActivity = new Intent();
            versCenterActivity.setClass(this, CenterActivity.class);
            startActivity(versCenterActivity);
            finish();

            //on met a jour la liste des dates du mois:
            if (sp.contains("DatesMois")){
                //alors il faut l'ajouter au bon endroit

                //on commence par recuperer les dates du mois enregistré
                String dateMoisStr=sp.getString("DatesMois", "");
                String[] listecharMois1 = dateMoisStr.split(" ");

                String[] listecharEntr = date.split("/");
                String DtintermEntr=listecharEntr[2]+listecharEntr[1];
                Integer dateEntrInt = Integer.parseInt(DtintermEntr);

                String[] listecharMois = listecharMois1[0].split("/");
                String DtintermMois=listecharMois[2]+listecharMois[1];
                Integer dateMoisInt = Integer.parseInt(DtintermMois);
                //(on ne regarde pas les jours, juste les mois et annee)

                if(dateEntrInt.equals(dateMoisInt)){
                    //alors la date entree est dans le meme mois que celles de la liste, on la rajoute si elle n y est pas deja
                    List<String> list = Arrays.asList(listecharMois1);
                    if (list.contains(date)){
                        //alors la date est deja ds la liste, il y a rien a faire
                    }
                    else{
                        //la date est dans le mois mais pas encore ds la liste
                        dateMoisStr= date+ " " +dateMoisStr;
                        editor.putString("DatesMois",dateMoisStr);
                        editor.apply();
                    }
                }
                else if (dateEntrInt>dateMoisInt){
                    //alors la date est plus recente et comme elle est valide, on doit etre passe ds un nouveau mois
                    //donc on initialise le nouveau mois avec cette date
                    editor.putString("DatesMois",date);
                    editor.apply();
                }
                else{
                    //on est dans un mois plus ancien que le dernier, on ne fait rien
                }
            }

        }
        else{
            Toast.makeText(this,"Veuillez entrer une date valide",Toast.LENGTH_SHORT).show();
        }



    }

    public void reviens(View view) {
        Intent lanceStart = new Intent();
        lanceStart.setClass(this, CenterActivity.class);
        startActivity(lanceStart);
        finish();
    }

    public void versEntreeTrajetActivity(View view) {
        enregistrerTrajet(view);
        Intent lanceEntreeTrajetActivity = new Intent();
        lanceEntreeTrajetActivity.setClass(this, entreeTrajetActivity.class);
        startActivity(lanceEntreeTrajetActivity);
    }

    public void entrerDate(View view) {

        TextView tvMarcheVelo = findViewById(R.id.textViewMarcheVelo);
        TextView tvBus = findViewById(R.id.textViewBus);
        TextView tvVoiture = findViewById(R.id.textViewVoiture);
        TextView tvTrainRERTram = findViewById(R.id.textViewTrainRERTram);
        TextView tvAvion = findViewById(R.id.textViewAvion);
        TextView tvConsigne = findViewById(R.id.textViewConsigne);

        EditText etMarcheVelo = findViewById(R.id.editTextMarcheVelo);
        EditText etBus = findViewById(R.id.editTextBus);
        EditText etVoiture = findViewById(R.id.editTextVoiture);
        EditText etTrainRERTram = findViewById(R.id.editTextTrainRERTram);
        EditText etAvion = findViewById(R.id.editTextAvion);
        EditText etDate = findViewById(R.id.editTextDate);

        Button bEnregistrer = findViewById(R.id.buttonEnregistrer);
        Button bMesTrajets = findViewById(R.id.buttonMesTrajets);

        SharedPreferences sp = getSharedPreferences("historique", Context.MODE_PRIVATE);

        String date = etDate.getText().toString();


        if (sp.contains(date)){

            String chaine = sp.getString(date,"0");

            Float[] liste;
            liste = CenterActivity.decode(chaine);

            Float kmMarcheVelo = liste[0];
            Float kmBus = liste[1];
            Float kmVoiture = liste[2];
            Float kmTrainRERTram = liste[3];
            Float kmAvion = liste[4];

            String strMarcheVelo = Float.toString(kmMarcheVelo);
            String strBus = Float.toString(kmBus);
            String strVoiture = Float.toString(kmVoiture);
            String strTrainRERTram = Float.toString(kmTrainRERTram);
            String strAvion = Float.toString(kmAvion);

            etMarcheVelo.setText(strMarcheVelo);
            etBus.setText(strBus);
            etVoiture.setText(strVoiture);
            etTrainRERTram.setText(strTrainRERTram);
            etAvion.setText(strAvion);
            tvConsigne.setText("Veuillez modifier le nombre de km effectué dans la journée");
            }

        else {
            etMarcheVelo.setText("");
            etBus.setText("");
            etVoiture.setText("");
            etTrainRERTram.setText("");
            etAvion.setText("");
            tvConsigne.setText("Veuillez saisir un nouveau trajet pour cette journée");
            Toast.makeText(this, "Aucun trajet enregistré pour cette date",Toast.LENGTH_LONG).show();
        }

        tvMarcheVelo.setVisibility(TextView.VISIBLE);
        tvBus.setVisibility(TextView.VISIBLE);
        tvVoiture.setVisibility(TextView.VISIBLE);
        tvTrainRERTram.setVisibility(TextView.VISIBLE);
        tvAvion.setVisibility(TextView.VISIBLE);
        tvConsigne.setVisibility(TextView.VISIBLE);
        etMarcheVelo.setVisibility(EditText.VISIBLE);
        etBus.setVisibility(EditText.VISIBLE);
        etVoiture.setVisibility(EditText.VISIBLE);
        etTrainRERTram.setVisibility(EditText.VISIBLE);
        etAvion.setVisibility(EditText.VISIBLE);
        bEnregistrer.setVisibility(Button.VISIBLE);
        bMesTrajets.setVisibility(Button.VISIBLE);
    }

    public static boolean verifDate(String date){
        String[] listechar = date.split("/");
        if (date.length()==10 && listechar.length==3){
            if (listechar[0].length()==2 && listechar[1].length()==2 && listechar[2].length()==4){
                if (listechar[0].matches("[+-]?\\d*(\\.\\d+)?") && listechar[1].matches("[+-]?\\d*(\\.\\d+)?") && listechar[2].matches("[+-]?\\d*(\\.\\d+)?")){
                    //on a regardé si les dates ont le bon format, on sait que le pgm ne plantera pas
                    //maintenant on regarde si la date est cohérente
                    String dateAct=CenterActivity.getDate();
                    String[] listecharAct = dateAct.split("/");
                    String DtintermAct=listecharAct[2]+listecharAct[1]+listecharAct[0];
                    Integer dateActInt = Integer.parseInt(DtintermAct);

                    String[] listecharEntr = date.split("/");
                    String DtintermEntr=listecharEntr[2]+listecharEntr[1]+listecharEntr[0];
                    Integer dateEntrInt = Integer.parseInt(DtintermEntr);

                    if (dateEntrInt<=dateActInt && Integer.parseInt(listecharEntr[0])<32 && Integer.parseInt(listecharEntr[1])<13){
                        //alors on a toutes les conditions pr que la date soit valide
                        return true;
                    }
                }
            }
        }
        return false;
    }
}

