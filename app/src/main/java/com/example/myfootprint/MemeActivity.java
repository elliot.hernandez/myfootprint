package com.example.myfootprint;

import static java.lang.String.valueOf;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class MemeActivity extends AppCompatActivity {

    LinearLayout monLL;
    ImageView monImage;
    Button leBout;

    public MediaPlayer Musica;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meme);

        //MUSICA !!!

        Musica=MediaPlayer.create(this,R.raw.musiquevictoire);
        Musica.setLooping(true);
        Musica.start();

        monLL=findViewById(R.id.monLL);
        leBout=findViewById(R.id.button4);
        int nb=(int) (Math.random()*17);
        if(nb==1) {
            //On récupère la vue pour la recréer après avoir fait un removeall
            ImageView vue1 = findViewById(R.id.imageView1);
            monLL.removeAllViews();
            monLL.addView(leBout);
            monLL.addView(vue1);
        }
        if(nb==2) {
            ImageView vue1 = findViewById(R.id.imageView2);
            monLL.removeAllViews();
            monLL.addView(leBout);
            monLL.addView(vue1);
        }
        if(nb==3) {
            ImageView vue1 = findViewById(R.id.imageView3);
            monLL.removeAllViews();
            monLL.addView(leBout);
            monLL.addView(vue1);
        }
        if(nb==4) {
            ImageView vue1 = findViewById(R.id.imageView4);
            monLL.removeAllViews();
            monLL.addView(leBout);
            monLL.addView(vue1);
        }
        if(nb==5) {
            ImageView vue1 = findViewById(R.id.imageView5);
            monLL.removeAllViews();
            monLL.addView(leBout);
            monLL.addView(vue1);
        }
        if(nb==6) {
            ImageView vue1 = findViewById(R.id.imageView6);
            monLL.removeAllViews();
            monLL.addView(leBout);
            monLL.addView(vue1);
        }
        if(nb==7) {
            ImageView vue1 = findViewById(R.id.imageView7);
            monLL.removeAllViews();
            monLL.addView(leBout);
            monLL.addView(vue1);
        }
        if(nb==8) {
            ImageView vue1 = findViewById(R.id.imageView8);
            monLL.removeAllViews();
            monLL.addView(leBout);
            monLL.addView(vue1);
        }
        if(nb==9) {
            ImageView vue1 = findViewById(R.id.imageView9);
            monLL.removeAllViews();
            monLL.addView(leBout);
            monLL.addView(vue1);
        }
        if(nb==10) {
            ImageView vue1 = findViewById(R.id.imageView10);
            monLL.removeAllViews();
            monLL.addView(leBout);
            monLL.addView(vue1);
        }
        if(nb==11) {
            ImageView vue1 = findViewById(R.id.imageView11);
            monLL.removeAllViews();
            monLL.addView(leBout);
            monLL.addView(vue1);
        }
        if(nb==12) {
            ImageView vue1 = findViewById(R.id.imageView12);
            monLL.removeAllViews();
            monLL.addView(leBout);
            monLL.addView(vue1);
        }
        if(nb==13) {
            ImageView vue1 = findViewById(R.id.imageView13);
            monLL.removeAllViews();
            monLL.addView(leBout);
            monLL.addView(vue1);
        }
        if(nb==14) {
            ImageView vue1 = findViewById(R.id.imageView14);
            monLL.removeAllViews();
            monLL.addView(leBout);
            monLL.addView(vue1);
        }
        if(nb==15) {
            ImageView vue1 = findViewById(R.id.imageView15);
            monLL.removeAllViews();
            monLL.addView(leBout);
            monLL.addView(vue1);
        }
        if(nb==16) {
            ImageView vue1 = findViewById(R.id.imageView16);
            monLL.removeAllViews();
            monLL.addView(leBout);
            monLL.addView(vue1);
        }
        if(nb==17) {
            ImageView vue1 = findViewById(R.id.imageView17);
            monLL.removeAllViews();
            monLL.addView(leBout);
            monLL.addView(vue1);
        }




    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("cycle","onStop");
        Musica.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("cycle","onResume");
        Musica.start();
    }

    public void rev(View view) {
        Intent lanceRev = new Intent();
        lanceRev.setClass(getApplicationContext(), CenterActivity.class);
        startActivity(lanceRev);
        Musica.stop();
        finish();
    }
}