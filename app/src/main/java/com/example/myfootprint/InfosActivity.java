package com.example.myfootprint;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.util.TypedValue;
import android.widget.LinearLayout;
import android.widget.TextView;

public class InfosActivity extends AppCompatActivity {
    LinearLayout ll;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_infos);

        ll=findViewById(R.id.llinfo);


        //titre de la page
        String n = "<font size =\"40\"><b>Informations sur l'application MyFootPrint</b></font> ";
        TextView tv1=new TextView(this);
        tv1.setTextSize(TypedValue.COMPLEX_UNIT_SP,30);
        tv1.setTextColor(Color.parseColor("#FF0000"));
        tv1.setText(Html.fromHtml(n));
        ll.addView(tv1);

        //espace
        n=" ";
        TextView tv16=new TextView(this);
        tv16.setText(n);
        ll.addView(tv16);

        //titre paragraphe 1
        n = "<font size =\"25\"><b>Philosophie globale de l’appli</b></font> ";
        TextView tv2=new TextView(this);
        tv2.setTextColor(Color.BLUE);
        tv2.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
        tv2.setText(Html.fromHtml(n));
        ll.addView(tv2);

        //texte paragraphe 1
        n = "Le but de MyFootPrint est de sensibiliser à l’impact écologique des transports. Cela passe par un bilan individuel des émissions dues aux transports et à sa représentation visuelle dans le contexte d’une utilisation durable : il faut réduire à 0,6 Planètes l’impact écologique des particuliers, les 0,4 Planètes restantes étant utilisées par les secteurs publics (écoles, institutions, infrastructure, …).";
        TextView tv3=new TextView(this);
        tv3.setText(Html.fromHtml(n));
        ll.addView(tv3);

        //espace
        n=" ";
        TextView tv17=new TextView(this);
        tv17.setText(n);
        ll.addView(tv17);

        //titre paragraphe 2 et 3
        n = "<font size =\"25\"><b>Principe du budget CO2</b></font> ";
        TextView tv4=new TextView(this);
        tv4.setTextColor(Color.BLUE);
        tv4.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
        tv4.setText(Html.fromHtml(n));
        ll.addView(tv4);

        //texte paragraphe 2
        n = "Comme chacun doit pouvoir mettre ses priorités sur des activités polluantes différentes, il serait assez contraignant de réguler pour quelles activités chacun a le droit de polluer. Par exemple, interdire la consommation de viande serait injuste envers celui qui ne se déplace qu’à vélo et vit dans une maison passive et dont le kif de la semaine est son steak le dimanche.";
        TextView tv5=new TextView(this);
        tv5.setText(Html.fromHtml(n));
        ll.addView(tv5);

        //texte paragraphe 3
        n="C’est pourquoi on utilise un <b>Budget CO2 par personne</b>, que chacun est libre de dépenser dans les activités qui lui tiennent le plus à cœur. La hauteur de ce budget est calculée grâce à l’empreinte carbone admissible par particulier pour une société durable, c’est-à-dire n’engendre pas d’augmentation de température globale, donc n’engendre pas une vague de migrants des zones devenues potentiellement mortelles (température > 37°C et aire saturée en humidité -> peut pas transpirer) vers celles encore viables (dont a priori l’Europe et les USA).";
        TextView tv6=new TextView(this);
        tv6.setText(Html.fromHtml(n));
        ll.addView(tv6);

        //espace
        n=" ";
        TextView tv18=new TextView(this);
        tv18.setText(n);
        ll.addView(tv18);

        //titre paragraphe 4
        n = "<font size =\"25\"><b>Calcul du budget CO2 par personne</b></font> ";
        TextView tv7=new TextView(this);
        tv7.setTextColor(Color.BLUE);
        tv7.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
        tv7.setText(Html.fromHtml(n));
        ll.addView(tv7);

        //texte paragraphe 4
        n="Comme susdit, il faut réduire à 0,6Planètes la consommation en ressources (donc aussi CO2 émis qui peut être absorbé) des particuliers. Compte tenu de l’évolution de la population, cela correspond à un <b>budget CO2 d’environ 2,2 tonnes de CO2 par personne par an</b> (source : <i>https://ree.developpement-durable.gouv.fr/themes/defis-environnementaux/changement-climatique/empreinte-carbone/article/l-empreinte-carbone-de-la-france</i>). ";
        TextView tv8=new TextView(this);
        tv8.setText(Html.fromHtml(n));
        ll.addView(tv8);

        //texte paragraphe 5
        n="On garde à partir de ce chiffre le pourcentage attitré aux transports en prenant le pourcentage actuel : (2,8tonnes/an) / (7,4tonnes/an) = 0,38. Le <b>budget Transport est donc de 0,836tonnesCO2/personne/an</b>. Ce procédé est déjà bien généreux pour les transports car cette branche est réputée comme étant parmi celles où le plus d’émissions peuvent être évitées. On remonte donc immédiatement aux <b>2,3KgCO2/personne/jour</b> ou aux 70kgCO2/personne/mois utilisés dans les calculs.";
        TextView tv9=new TextView(this);
        tv9.setText(Html.fromHtml(n));
        ll.addView(tv9);

        //espace
        n=" ";
        TextView tv19=new TextView(this);
        tv19.setText(n);
        ll.addView(tv19);

        //titre paragraphe 6 et 7 et 8
        n = "<font size =\"25\"><b>Calcul des émissions par moyen de transport</b></font> ";
        TextView tv10=new TextView(this);
        tv10.setTextColor(Color.BLUE);
        tv10.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
        tv10.setText(Html.fromHtml(n));
        ll.addView(tv10);

        //texte paragraphe 6
        n="Ces émissions sont des moyennes des différentes formes et utilisations de chaque moyen de transport. Ces chiffres sont seulement significatifs à cause de ces <b>imprécisions dues aux entrées simples</b> pour l’utilisateur. Ces données ont étés récupérées sur <i>https://www.greenly.earth/blog/empreinte-carbone-comparatif-transports</i>.";
        TextView tv11=new TextView(this);
        tv11.setText(Html.fromHtml(n));
        ll.addView(tv11);

        //texte paragraphe 7
        n="Ainsi nous avons pris : pied/vélo : 0,2 gCO2/km ; bus : 75g/km ; voiture : 125g/km ; train : 10g/km ; avion : 215g/km.";
        TextView tv12=new TextView(this);
        tv12.setText(Html.fromHtml(n));
        ll.addView(tv12);

        //texte paragraphe 8
        n="Surtout pour le train, on a en réalité : TGV : 2,4g/km ; intercity : 8,1g/km ; TER : 29,4g/km. Le 10g/km est donc plus qu’approximatif.";
        TextView tv13=new TextView(this);
        tv13.setText(Html.fromHtml(n));
        ll.addView(tv13);

        //espace
        n=" ";
        TextView tv20=new TextView(this);
        tv20.setText(n);
        ll.addView(tv20);

        //titre paragraphe 9
        n = "<font size =\"25\"><b>Conclusion</b></font> ";
        TextView tv14=new TextView(this);
        tv14.setTextColor(Color.BLUE);
        tv14.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
        tv14.setText(Html.fromHtml(n));
        ll.addView(tv14);

        //texte paragraphe 9
        n="En utilisant cette appli, l’utilisateur doit prendre conscience de l’impact de chacun de ses trajets, doit comprendre quels sont les transports à privilégier et reçoit un retour positif s'il n’a pas dépassé le budget Co2 de la journée attitré aux transports sous forme d’un meme tiré au hasard. Cette méthode réputée comme un des rouage de l'addiction aux jeux, irait le motiver encore davantage à respecter son budget.";
        TextView tv15=new TextView(this);
        tv15.setText(Html.fromHtml(n));
        ll.addView(tv15);
    }
}