package com.example.myfootprint;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Map;

public class entreeTrajetActivity extends AppCompatActivity {
    private LinearLayout l1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entree_trajet);

        l1=findViewById(R.id.llhome);

        TextView lequel=new TextView(this);
        String n = "<b><font size =\\\"20\\\">Tu as pris quel trajet?</font></b>";
        //peut aussi textView.setTextSize(X) avec X interprete en sp
        //ou setTextSize(TypedValue.COMPLEX_UNIT_DIP, X) avec X en dp
        lequel.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
        lequel.setText(Html.fromHtml(n));
        l1.addView(lequel);

        TextView espace1=new TextView(this);
        espace1.setText(" ");
        l1.addView(espace1);

        //on affiche autant de boutons qu'il y a de trajets préenregistrés
        SharedPreferences sp=getSharedPreferences("TrajetsPreenregistres", Context.MODE_PRIVATE);
        String tsTrajets = sp.getString("tsTrajets", "");

        String[] listechar = tsTrajets.split(" ");
        if (tsTrajets=="") {
            Toast.makeText(this, "Il n'y a aucun trajet préenregistré...", Toast.LENGTH_SHORT).show();
            //On n'affiche aucun bouton: pas de trajet préenregistré
        }
        else {
            for (int q = 0; q < listechar.length; q++) {
                Button but = new Button(this);
                but.setText(listechar[q]);
                but.setBackgroundColor(-65281);
                but.setTextColor(Color.WHITE);
                but.setOnClickListener(goz);
                l1.addView(but);
                TextView espace=new TextView(this);
                espace.setText(" ");
                l1.addView(espace);
            }
        }
    }

    private View.OnClickListener goz= new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Button boutonClique = (Button) v;
            String trajetSelect = boutonClique.getText().toString();
            Intent messageVersTrajetActivity = new Intent();
            messageVersTrajetActivity.setClass(getApplicationContext(),TrajetActivity.class);
            messageVersTrajetActivity.putExtra("messageVersTrajetActivity",trajetSelect);
            //a recuperer ds TrajetActivity et a chercher les bonnes valeurs
            // ou alors met direct code pr que enregistre comme ce que ferait TrajetActivity
            // + toat avec trajet pris en compte ou dans le genre
            startActivity(messageVersTrajetActivity);
            finish();
        }
    };


}