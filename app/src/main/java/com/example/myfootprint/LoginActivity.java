package com.example.myfootprint;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getIntent();
    }

    public void startCenterActivity(View view) {
        EditText et = findViewById(R.id.editTextPseudo);
        String pseudo = et.getText().toString();
        if  (pseudo.isEmpty()) {
            Toast.makeText(this, "Veuillez entrer un pseudo", Toast.LENGTH_SHORT).show();
        }
        else {
            SharedPreferences sp = getSharedPreferences("historique", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("pseudo", pseudo);
            editor.apply();

            Intent lanceStart = new Intent();
            lanceStart.setClass(this, CenterActivity.class);
            startActivity(lanceStart);
            finish();
        }
    }
}