package com.example.myfootprint;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.icu.util.Calendar;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.myfootprint.databinding.ActivityCenterBinding;
import com.example.myfootprint.ui.ConfirmActivity;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class CenterActivity extends AppCompatActivity {


    private AppBarConfiguration mAppBarConfiguration;
    private ActivityCenterBinding binding;
    private Calendar cal;
    private double coeffMarcheVelo = 0.0002;
    private double coeffBus = 0.070;
    private double coeffVoiture = 0.125;
    private double coeffTrainRERTram = 0.01;
    private double coeffAvion = 0.215;
    private double scrTotal;
    public LinearLayout lbis;
    public MediaPlayer Musica;
    TextView traj;
    TextView etMarcheVelo;
    TextView etBus;
    TextView etVoiture;
    TextView etTrainRERTram;
    TextView etAvion;
    Button supprimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Temporaire pour skip le login
        Intent consRev = getIntent();

        Musica=MediaPlayer.create(this,R.raw.sad);


        binding = ActivityCenterBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.appBarCenter.toolbar);

        traj = new TextView(this);
        etMarcheVelo = new TextView(this);
        etBus = new TextView(this);
        etVoiture = new TextView(this);
        etTrainRERTram = new TextView(this);
        etAvion = new TextView(this);
        supprimer = new Button(this);

        binding.appBarCenter.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent lanceStart = new Intent();
                lanceStart.setClass(getApplicationContext(), TrajetActivity.class);
                lanceStart.putExtra("messageVersTrajetActivity","");
                startActivity(lanceStart);
            }
        });
        binding.appBarCenter.fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(scrTotal < 2.3 ) {
                    Intent lanceStart = new Intent();
                    lanceStart.setClass(getApplicationContext(), MemeActivity.class);
                    Toast.makeText(getApplicationContext(),"C'est bien",Toast.LENGTH_SHORT).show();
                    startActivity(lanceStart);
                }
                else {
                    Musica.start();
                    Snackbar.make(view,"Revoit tes moyens de locomotion",Snackbar.LENGTH_SHORT).show();
                }
            }

        });

        DrawerLayout drawer = binding.drawerLayout;
        NavigationView navigationView = binding.navView;
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow,R.id.nav_maps,R.id.nav_mesTrajets)
                .setOpenableLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_center);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.center, menu);
        return true;
    }

    public void startInfos(MenuItem item) {
        Intent lanceSettingsActivity = new Intent();
        lanceSettingsActivity.setClass(getApplicationContext(), InfosActivity.class);
        startActivity(lanceSettingsActivity);
    }
    public void reinitialiserPseudo(View view) {
        Intent messageVersConfirmActivity = new Intent();
        messageVersConfirmActivity.setClass(this, ConfirmActivity.class);
        startActivity(messageVersConfirmActivity);
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_center);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    public void goTrajetMethod() {
        Intent lanceStart = new Intent();
        lanceStart.setClass(this, TrajetActivity.class);
        startActivity(lanceStart);
    }

    public void goSubNewTraj() {
        String nomTrajet;
        Float KMpied;
        Float KMbus;
        Float KMvoit;
        Float KMtrain;
        Float KMavion;

        //recupere entrees de champ txt
        EditText etNomTrajet = findViewById(R.id.editTextNomTrajet);
        //on vérifie qu'il y a bien un nom pour le trajet
        if (etNomTrajet.getText().toString().isEmpty()){
            Toast.makeText(this,"Veuillez entrer un nom sans espaces",Toast.LENGTH_LONG).show();
        }
        else {
            nomTrajet = etNomTrajet.getText().toString();

            //on verifie aussi qu il y a pas d espaces, psk ca fait planter
            String[] nomTrajTab= nomTrajet.split(" ");
            if (nomTrajTab.length>1){
                Toast.makeText(this,"Veuillez entrer un nom sans espaces",Toast.LENGTH_LONG).show();
            }
            else{
                //alors on sait que le nom existe et n a pas d espaces
                EditText etKMpied = findViewById(R.id.editTextMarcheVelo);
                EditText etKMbus = findViewById(R.id.editTextBus);
                EditText etKMvoit = findViewById(R.id.editTextVoiture);
                EditText etKMtrain = findViewById(R.id.editTextTrainRERTram);
                EditText etKMavion = findViewById(R.id.editTextAvion);


                //on définie les valeurs par défaut
                if(etKMpied.getText().toString().isEmpty()){
                    KMpied=0F;
                } else {
                    KMpied = Float.parseFloat(etKMpied.getText().toString());
                }
                if(etKMbus.getText().toString().isEmpty()){
                    KMbus=0F;
                } else {
                    KMbus = Float.parseFloat(etKMbus.getText().toString());
                }
                if(etKMvoit.getText().toString().isEmpty()){
                    KMvoit=0F;
                } else {
                    KMvoit = Float.parseFloat(etKMvoit.getText().toString());
                }
                if(etKMtrain.getText().toString().isEmpty()){
                    KMtrain=0F;
                } else {
                    KMtrain = Float.parseFloat(etKMtrain.getText().toString());
                }
                if(etKMavion.getText().toString().isEmpty()){
                    KMavion=0F;
                } else {
                    KMavion = Float.parseFloat(etKMavion.getText().toString());
                }
                //fin recuperation

                //on enregistre ca ds le doc "TrajetsPreenregistres" ss forme String a split pr recuperer les donnees
                SharedPreferences sp = getSharedPreferences("TrajetsPreenregistres", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();

                //enregistrement/mise a jour de l enregistrement de ts les trajets entres par l utilisateur
                String tsTrajets;
                if (sp.contains("tsTrajets")) {
                    String[] tsTrajetsChar = sp.getString("tsTrajets", "").split(" ");
                    List<String> list = Arrays.asList(tsTrajetsChar);
                    if (list.contains(nomTrajet)) {
                        //Ne fait rien: on remplace un trajet déjà existant
                    } else {
                        tsTrajets = sp.getString("tsTrajets", "") + " " + nomTrajet;
                        editor.putString("tsTrajets", tsTrajets);
                        editor.apply();
                    }
                } else {
                    editor.putString("tsTrajets", nomTrajet);
                    editor.apply();
                }
                //fin de l enregistrement/mise a jour de ts les trajets

                String entree = KMpied.toString() + " " + KMbus.toString() + " " + KMvoit.toString() + " " + KMtrain.toString() + " " + KMavion.toString();
                editor.putString(nomTrajet, entree);
                editor.apply();

                Intent versCenterActivity = new Intent();
                versCenterActivity.setClass(this, CenterActivity.class);
                startActivity(versCenterActivity);
            }

        }
    }

    public static Float[] decode(String chaine) {
        //prend une chaine de valeurs séparées par des espaces et renvoit un tableau de float avec les valeurs dedans
        //elle sert a stocker le nb de km pour chaque moyen de transport sans utiliser une database
        //(info stockees sous cette forme : key="JJ/MM/AAAA" "km_MarcheVelo km_Bus km_Voiture km_Train km_Avion")

        String[] listechar = chaine.split(" ");
        Float[] liste = new Float[listechar.length];
        for (int k = 0; k < listechar.length; k++) {
            liste[k] = Float.parseFloat(listechar[k]);
        }
        return liste;
    }

    // fonction utilisee dans calculScoreTotal qui fait le produit de 2 double et qui arrondit le resultats au millieme
    public double calculScore(Float nbKM, double coeff) {
        return Math.round(nbKM * coeff * 1000.0)/1000.0;
    }

    public double[] calculScoreTotal(String date) {
        SharedPreferences sp = getSharedPreferences("historique", Context.MODE_PRIVATE);
        String strValeurs = sp.getString(date, "0.0 0.0 0.0 0.0 0.0");
        Float[] listeValeurs = decode(strValeurs);

        Float kmMarcheVelo = listeValeurs[0];
        Float kmBus = listeValeurs[1];
        Float kmVoiture = listeValeurs[2];
        Float kmTrainRERTram = listeValeurs[3];
        Float kmAvion = listeValeurs[4];

        double scrMarcheVelo = calculScore(kmMarcheVelo, coeffMarcheVelo);
        double scrBus = calculScore(kmBus, coeffBus);
        double scrVoiture = calculScore(kmVoiture, coeffVoiture);
        double scrTrainRERTram = calculScore(kmTrainRERTram, coeffTrainRERTram);
        double scrAvion = calculScore(kmAvion, coeffAvion);

        scrTotal = Math.round((scrMarcheVelo + scrBus + scrVoiture + scrTrainRERTram + scrAvion)*1000.0)/1000.0;

        return new double[]{scrTotal, scrMarcheVelo, scrBus, scrVoiture, scrTrainRERTram, scrAvion};
    }

    public static String getDate() {

        //Date currentTime = Calendar.getInstance().getTime();  //methode qui a pas marche

        String currentDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());

        //cal = Calendar.getInstance();  //aussi methode qui a pas marche
        //String sDate = cal.get(Calendar.DAY_OF_MONTH) + "/" + cal.get(Calendar.MONTH) + "/" + cal.get(Calendar.YEAR);
        //return sDate;

        //on change le format de dd-mm-yyyy a dd/mm/yyyy
        String[] listechar = currentDate.split("-");
        currentDate=listechar[0]+"/"+listechar[1]+"/"+listechar[2];
        return currentDate;
    }


    //fonctions recuperer date
    public static int getLengthDecode (String chaine) {
        //prend une chaine de valeurs séparées par des espaces et renvoit sa longeur
        String[] listechar = chaine.split(" ");
        return listechar.length;
    }

    public static String[] Dates7der (String chaine) {
        //prend une chaine de valeurs séparées par des espaces et renvoit un tableau de String avec les valeurs dedans
        String[] listechar = chaine.split(" ");
        return listechar;
    }

    public static void triDates(String[] dates){
        //prend la liste de 7 dates et les trie IN PLACE selon la plus recente a la moins recente
        Integer[] nbDates=new Integer[dates.length];
        String Dtinterm;
        String zerosAjout="";
        for (int r=0;r<dates.length; r++){
            String[] listechar = dates[r].split("/");
            Dtinterm=listechar[2]+listechar[1]+listechar[0];
            nbDates[r]= Integer.parseInt(Dtinterm);
        }
        //maintenant on a une liste de int qu on peut trier
        Arrays.sort(nbDates, Collections.reverseOrder());

        //il retse a reconvertir ce tableau en dates
        for (int r=0;r<dates.length; r++){
            Dtinterm=nbDates[r].toString();
            //si il y avait des 0 a l annee, ils on ete virés par le passage au int -> on les remet
            zerosAjout="";
            for (int q=0;q<8-Dtinterm.length(); q++){
                zerosAjout=zerosAjout+"0";
            }
            Dtinterm=zerosAjout+Dtinterm;
            dates[r]= Dtinterm.substring(6, 8)+"/"+ Dtinterm.substring(4, 6)+"/"+Dtinterm.substring(0, 4);
        //on a rerempli le tableau date avec les dates dans le bon ordre
        }
    }

    //fin récupérer date

    //On affiche les trajets préenregistrés
    public final View.OnClickListener affiche = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Button boutonClique = (Button) v;
            String trajetSelect = boutonClique.getText().toString();

            try {
                lbis = new LinearLayout(getApplicationContext());
                //On veut qu'il soit vertical
                lbis.setOrientation(LinearLayout.VERTICAL);
                LinearLayout l1 = findViewById(R.id.llmesTrajets);
                l1.addView(lbis);
            } catch (Exception e) {

            }

            try {
                lbis = new LinearLayout(getApplicationContext());
                //On veut qu'il soit vertical
                lbis.setOrientation(LinearLayout.VERTICAL);
                LinearLayout l1 = findViewById(R.id.llmesTrajets);
            } catch (Exception e) {

            }

            try {
                //On veut qu'il soit vertical
                lbis.setOrientation(LinearLayout.VERTICAL);
                LinearLayout l1 = findViewById(R.id.llmesTrajets);
                l1.addView(lbis);
            } catch (Exception e) {

            }

            try {
                // parse la réponse JSON reçue
                lbis.removeView(traj);
                lbis.removeView(supprimer);
                lbis.removeView(etMarcheVelo);
                lbis.removeView(etBus);
                lbis.removeView(etVoiture);
                lbis.removeView(etTrainRERTram);
                lbis.removeView(etAvion);

            } catch (Exception e) {

            }


            //On récupère la donnée
            SharedPreferences mesTrajets = getSharedPreferences("TrajetsPreenregistres", Context.MODE_PRIVATE);
            String trajetChoisi = mesTrajets.getString(trajetSelect, "");

            //On casse la donnée brute
            Float[] kmTrajet = CenterActivity.decode(trajetChoisi);

            Float kmMarcheVelo = kmTrajet[0];
            Float kmBus = kmTrajet[1];
            Float kmVoiture = kmTrajet[2];
            Float kmTrainRERTram = kmTrajet[3];
            Float kmAvion = kmTrajet[4];

            String strMarcheVelo = Float.toString(kmMarcheVelo);
            String strBus = Float.toString(kmBus);
            String strVoiture = Float.toString(kmVoiture);
            String strTrainRERTram = Float.toString(kmTrainRERTram);
            String strAvion = Float.toString(kmAvion);

            //On set texte et on afffice
            traj.setText(trajetSelect);
            etMarcheVelo.setText("Distance en Marche ou Vélo: " + strMarcheVelo + " km");
            etBus.setText("Distance en Bus: " + strBus + " km");
            etVoiture.setText("Distance en voiture: " + strVoiture + " km");
            etTrainRERTram.setText("Distance en Train ou Tram ou RER: " + strTrainRERTram + " km");
            etAvion.setText("Distance en Avion: " + strAvion + " km");
            supprimer.setText("Supprimer ce trajet: " + trajetSelect );
            supprimer.setOnClickListener(supprime);


            try {
                lbis.addView(traj);
                lbis.addView(supprimer);
                lbis.addView(etMarcheVelo);
                lbis.addView(etBus);
                lbis.addView(etVoiture);
                lbis.addView(etTrainRERTram);
                lbis.addView(etAvion);
            } catch (Exception e) {

            }


        }
    };

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void setCouleurProgressBar(ProgressBar progressBar, int percent) {
        // Fonction qui change la couleur de la progress bar en fonction du score et qui lui indique jusqu'a combien elle doit etre 'remplie'
        // definie dans centerAcitivity car utilisee dans plusieurs activites differentes
        if (percent<=8) {
            progressBar.setProgressTintList(ColorStateList.valueOf(Color.rgb(0,100,0)));
        }
        else if (percent<=16){
            progressBar.setProgressTintList(ColorStateList.valueOf(Color.rgb(34,139,34)));
        }
        else if (percent<=24){
            progressBar.setProgressTintList(ColorStateList.valueOf(Color.rgb(46,139,87)));
        }
        else if (percent<=32) {
            progressBar.setProgressTintList(ColorStateList.valueOf(Color.rgb(60, 179, 113)));
        }
        else if (percent<=40){
            progressBar.setProgressTintList(ColorStateList.valueOf(Color.rgb(144,238,144)));
        }
        else if (percent<=48){
            progressBar.setProgressTintList(ColorStateList.valueOf(Color.rgb(173,255,47)));
        }
        else if (percent<=56){
            progressBar.setProgressTintList(ColorStateList.valueOf(Color.rgb(255,215,0)));
        }
        else if (percent<=64){
            progressBar.setProgressTintList(ColorStateList.valueOf(Color.rgb(255,165,0)));
        }
        else if (percent<=72){
            progressBar.setProgressTintList(ColorStateList.valueOf(Color.rgb(255,140,0)));
        }
        else if (percent<=80){
            progressBar.setProgressTintList(ColorStateList.valueOf(Color.rgb(255,99,71)));
        }
        else if (percent<=88){
            progressBar.setProgressTintList(ColorStateList.valueOf(Color.rgb(255,69,0)));
        }
        else {
            progressBar.setProgressTintList(ColorStateList.valueOf(Color.rgb(139,0,0)));
        }
        progressBar.setProgress(percent);
    }
    //Fin affichage trajet preenregistré
    //A FAIRE
    //A FAIRE
    //On récupère le texte du boutou et on coupe pour n'avoir que le nom du trajet et le supprimer
    //A FAIRE
    //A FAIRE
    public View.OnClickListener supprime=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Button boutonClique = (Button) v;
            String texteBout = boutonClique.getText().toString();
            String[] variabl= texteBout.split(": ");
            String trajetSelect=variabl[1];

            //on vire le trajet du fichier et de la liste ts trajets
            SharedPreferences sp=getSharedPreferences("TrajetsPreenregistres", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor=sp.edit();
            editor.remove(trajetSelect);
            editor.apply();
            //mntnt on prend la liste tsTrajets et on le vire de là
            String tsTrajets = sp.getString("tsTrajets", "");
            String[] listechar = tsTrajets.split(" ");
            String[] listesort= new String[listechar.length-1];
            int r=0;
            for (int f=0;f<listechar.length;f++){
                if (listechar[f].equals(trajetSelect)){
                    //on ne fait rien car c celui qu'on veut virer
                }
                else{
                    listesort[r]=listechar[f];
                    r=r+1;
                }
            }
            //on a mntnt un tableau sans l element a virer, on le reconvertit en String:
            String newTsTraj="";
            for (int g=0;g<listesort.length;g++){
                newTsTraj=newTsTraj+listesort[g] + " ";
            }
            editor.putString("tsTrajets",newTsTraj);
            editor.apply();

            Toast.makeText(getApplicationContext(),"trajet "+ trajetSelect+ " effacé. Veuillez recharger la page.",Toast.LENGTH_SHORT).show();



        }
    };

}