package com.example.myfootprint;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class StartActivity extends AppCompatActivity {
    int index=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
        executorService.scheduleAtFixedRate(new Runnable() {
            public void run() {
                if (index == 1) {
                    lanceNext();
                    executorService.shutdown();
                }
                else{
                    index=index+1;
                }
            }
        }, 0, 1, TimeUnit.SECONDS);

    }

    private void lanceNext(){
        SharedPreferences sp = getSharedPreferences("historique", Context.MODE_PRIVATE);

        //on initialise DatesMois et historique avec une date vide (que des 0)
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("00/00/0000","0.0 0.0 0.0 0.0 0.0");
        editor.apply();
        if (sp.contains("DatesMois")){
            //on ne fait rien, le dossier est deja initialisé
        }
        else{
            editor.putString("DatesMois","00/00/0000");
            editor.apply();
        }

        if (sp.contains("pseudo")) {
            //Temporaire pour skip le login
            Intent lanceStart = new Intent();
            lanceStart.setClass(this, CenterActivity.class);
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            startActivity(lanceStart);
            finish();
            //Fin Temp
        }
        else {
            Intent lanceSignUp = new Intent();
            lanceSignUp.setClass(this, LoginActivity.class);
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            startActivity(lanceSignUp);
            finish();
        }
    }

}
