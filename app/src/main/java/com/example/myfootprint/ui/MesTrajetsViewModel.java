package com.example.myfootprint.ui;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class MesTrajetsViewModel extends ViewModel {
    private MutableLiveData<String> mText;

    public MesTrajetsViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is mesTrajtes fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}