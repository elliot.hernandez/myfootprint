package com.example.myfootprint.ui.home;

import static java.lang.Double.valueOf;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.myfootprint.CenterActivity;
import com.example.myfootprint.R;
import com.example.myfootprint.databinding.FragmentHomeBinding;

import java.io.File;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private FragmentHomeBinding binding;
    private double coeffMarcheVelo = 0.0002;
    private double coeffBus = 0.070;
    private double coeffVoiture = 0.125;
    private double coeffTrainRERTram = 0.01;
    private double coeffAvion = 0.215;
    private double scoreCarboneJour;
    private double totalCarboneJour = 2.30;
    private double totalCarboneMois = 70;

    @RequiresApi(api = Build.VERSION_CODES.P)
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);

        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        ProgressBar progressBar = root.findViewById(R.id.progressBar);

        TextView tvPseudo = root.findViewById(R.id.textViewPseudo);
        TextView tvAffichageScore = root.findViewById(R.id.textViewAffichageScore);

        TextView tvMarcheVeloKM = root.findViewById(R.id.textViewMarcheVeloKM);
        TextView tvMarcheVeloPTS = root.findViewById(R.id.textViewMarcheVeloPTS);

        TextView tvBusKM = root.findViewById(R.id.textViewBusKM);
        TextView tvBusPTS = root.findViewById(R.id.textViewBusPTS);

        TextView tvVoitureKM = root.findViewById(R.id.textViewVoitureKM);
        TextView tvVoiturePTS = root.findViewById(R.id.textViewVoiturePTS);

        TextView tvTrainRERTramKM = root.findViewById(R.id.textViewTrainRERTramKM);
        TextView tvTrainRERTramPTS = root.findViewById(R.id.textViewTrainRERTramPTS);

        TextView tvAvionKM = root.findViewById(R.id.textViewAvionKM);
        TextView tvAvionPTS = root.findViewById(R.id.textViewAvionPTS);

        //recupere pseudo
        SharedPreferences sp= getActivity().getSharedPreferences("historique", Context.MODE_PRIVATE);
        String pseudo =sp.getString("pseudo", "nobody");
        //fin recup pseudo

        //recupere date du tel
        String date = ((CenterActivity) getActivity()).getDate();
        String chaine = sp.getString(date, "0 0 0 0 0");

        Float[] liste;
        liste = ((CenterActivity) getActivity()).decode(chaine);

        Float kmMarcheVelo = liste[0];
        Float kmBus = liste[1];
        Float kmVoiture = liste[2];
        Float kmTrainRERTram = liste[3];
        Float kmAvion = liste[4];

        String strMarcheVelo = Float.toString(kmMarcheVelo);
        String strBus = Float.toString(kmBus);
        String strVoiture = Float.toString(kmVoiture);
        String strTrainRERTram = Float.toString(kmTrainRERTram);
        String strAvion = Float.toString(kmAvion);

        scoreCarboneJour = ((CenterActivity) getActivity()).calculScoreTotal(date)[0];
        tvPseudo.setText("Pseudo : " + pseudo);
        tvAffichageScore.setText(scoreCarboneJour + " kgCO2 / " + totalCarboneJour + " kgCO2");

        tvMarcheVeloKM.setText(strMarcheVelo + " km");
        tvMarcheVeloPTS.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmMarcheVelo, coeffMarcheVelo)) + " kgCO2");

        tvBusKM.setText(strBus + " km");
        tvBusPTS.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmBus, coeffBus)) + " kgCO2");

        tvVoitureKM.setText(strVoiture + " km");
        tvVoiturePTS.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmVoiture, coeffVoiture)) + " kgCO2");

        tvTrainRERTramKM.setText(strTrainRERTram + " km");
        tvTrainRERTramPTS.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmTrainRERTram, coeffTrainRERTram)) + " kgCO2");

        tvAvionKM.setText(strAvion + " km");
        tvAvionPTS.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmAvion, coeffAvion)) + " kgCO2");

        double quotient = (scoreCarboneJour/totalCarboneJour);
        double  doublePercent = quotient*100;
        int percent = (int) doublePercent;

        ((CenterActivity) getActivity()).setCouleurProgressBar(progressBar,percent);

        //AFFICHAGE DU SCORE DU MOIS

        ProgressBar progressBarMois = root.findViewById(R.id.progressBarMois);
        TextView tvAffichageScoreMois = root.findViewById(R.id.textViewAffichageScoreMois);

        TextView tvMarcheVeloKMMois = root.findViewById(R.id.textViewMarcheVeloKMMois);
        TextView tvMarcheVeloPTSMois = root.findViewById(R.id.textViewMarcheVeloPTSMois);

        TextView tvBusKMMois = root.findViewById(R.id.textViewBusKMMois);
        TextView tvBusPTSMois = root.findViewById(R.id.textViewBusPTSMois);

        TextView tvVoitureKMMois = root.findViewById(R.id.textViewVoitureKMMois);
        TextView tvVoiturePTSMois = root.findViewById(R.id.textViewVoiturePTSMois);

        TextView tvTrainRERTramKMMois = root.findViewById(R.id.textViewTrainRERTramKMMois);
        TextView tvTrainRERTramPTSMois = root.findViewById(R.id.textViewTrainRERTramPTSMois);

        TextView tvAvionKMMois = root.findViewById(R.id.textViewAvionKMMois);
        TextView tvAvionPTSMois = root.findViewById(R.id.textViewAvionPTSMois);

        String chaineDatesMois = sp.getString("DatesMois", "00/00/0000");
        String[] listeDatesMois = chaineDatesMois.split(" ");

        double scoreMois = 0;
        double scoreMoisMarcheVelo = 0;
        double scoreMoisBus = 0;
        double scoreMoisVoiture = 0;
        double scoreMoisTrainRERTram = 0;
        double scoreMoisAvion = 0;
        double kmMoisMarcheVelo = 0;
        double kmMoisBus = 0;
        double kmMoisVoiture = 0;
        double kmMoisTrainRERTram = 0;
        double kmMoisAvion = 0;

        String ScoreJr;

        for (int i = 0; i<listeDatesMois.length; i++) {
            String strDate = listeDatesMois[i];
            //on recupere le kilometrage correspondant a cette date:
            ScoreJr=sp.getString(strDate,"0.0 0.0 0.0 0.0 0.0");
            //ev faut d abord initialiser la lgeur -> remplace cette ligne
            //Float[] listeScoreMois = ((CenterActivity) getActivity()).decode(strDate);
            //par:
            String[] listechar = ScoreJr.split(" ");
            Float[] listeScoreJr = new Float[listechar.length];
            for (int k = 0; k < listechar.length; k++) {
                listeScoreJr[k] = Float.parseFloat(listechar[k]);
            }
            Float kmMV = listeScoreJr[0];
            Float kmB = listeScoreJr[1];
            Float kmV = listeScoreJr[2];
            Float kmTRERT = listeScoreJr[3];
            Float kmA = listeScoreJr[4];

            double scoreJour = ((CenterActivity) getActivity()).calculScoreTotal(strDate)[0];
            double scoreJourMarcheVelo = ((CenterActivity) getActivity()).calculScoreTotal(strDate)[1];
            double scoreJourBus = ((CenterActivity) getActivity()).calculScoreTotal(strDate)[2];
            double scoreJourVoiture = ((CenterActivity) getActivity()).calculScoreTotal(strDate)[3];
            double scoreJourTrainRERTram = ((CenterActivity) getActivity()).calculScoreTotal(strDate)[4];
            double scoreJourAvion = ((CenterActivity) getActivity()).calculScoreTotal(strDate)[5];

            scoreMois = scoreMois + scoreJour;
            scoreMoisMarcheVelo = scoreMoisMarcheVelo + scoreJourMarcheVelo;
            scoreMoisBus = scoreMoisBus + scoreJourBus;
            scoreMoisVoiture = scoreMoisVoiture + scoreJourVoiture;
            scoreMoisTrainRERTram = scoreMoisTrainRERTram + scoreJourTrainRERTram;
            scoreMoisAvion = scoreMoisAvion + scoreJourAvion;

            kmMoisMarcheVelo = kmMoisMarcheVelo + kmMV;
            kmMoisBus = kmMoisBus + kmB;
            kmMoisVoiture = kmMoisVoiture + kmV;
            kmMoisTrainRERTram = kmMoisTrainRERTram + kmTRERT;
            kmMoisAvion = kmMoisAvion + kmA;
        }

        double quotientMois = (scoreMois/totalCarboneMois);
        double  doublePercentMois = quotientMois*100;
        int percentMois = (int) doublePercentMois;

        scoreMois = Math.round(scoreMois * 1000.0)/1000.0;
        ((CenterActivity) getActivity()).setCouleurProgressBar(progressBarMois,percentMois);
        tvAffichageScoreMois.setText(scoreMois + " kgCO2 / " + totalCarboneMois + " kgCO2");

        tvMarcheVeloKMMois.setText(kmMoisMarcheVelo + " km");
        tvMarcheVeloPTSMois.setText(scoreMoisMarcheVelo + " kgCO2");

        tvBusKMMois.setText(kmMoisBus + " km");
        tvBusPTSMois.setText(scoreMoisBus + " kgCO2");

        tvVoitureKMMois.setText(kmMoisVoiture + " km");
        tvVoiturePTSMois.setText(scoreMoisVoiture + " kgCO2");

        tvTrainRERTramKMMois.setText(kmMoisTrainRERTram + " km");
        tvTrainRERTramPTSMois.setText(scoreMoisTrainRERTram + " kgCO2");

        tvAvionKMMois.setText(kmMoisAvion + " km");
        tvAvionPTSMois.setText(scoreMoisAvion + " kgCO2");
        //fin affiche score mois


        return root;
    }

    private View.OnClickListener goTrajet= new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ((CenterActivity) getActivity()).goTrajetMethod();
        }
    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}