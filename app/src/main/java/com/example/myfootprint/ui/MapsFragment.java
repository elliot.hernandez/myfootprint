package com.example.myfootprint.ui;

import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.myfootprint.R;
import com.example.myfootprint.databinding.MapsFragmentBinding;

public class MapsFragment extends Fragment {

    private MapsViewModel mViewModel;
    private Object MapsViewModel;
    private MapsFragmentBinding binding;

    public static MapsFragment newInstance() {
        return new MapsFragment();
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        MapsViewModel =
                new ViewModelProvider(this).get(MapsViewModel.class);

        binding = MapsFragmentBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        WebView myWebView=root.findViewById(R.id.webView);
        myWebView.setWebViewClient(new WebViewClient());
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.loadUrl("https://www.google.fr/maps");

        return root;
    }

}