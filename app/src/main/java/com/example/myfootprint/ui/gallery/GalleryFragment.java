package com.example.myfootprint.ui.gallery;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.myfootprint.CenterActivity;
import com.example.myfootprint.R;
import com.example.myfootprint.databinding.FragmentGalleryBinding;

public class GalleryFragment extends Fragment {

    private GalleryViewModel galleryViewModel;
    private FragmentGalleryBinding binding;
    Button enre;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        galleryViewModel =
                new ViewModelProvider(this).get(GalleryViewModel.class);

        binding = FragmentGalleryBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        enre=root.findViewById(R.id.button0);
        enre.setOnClickListener(SubNewTraj);

        return root;
    }

    private View.OnClickListener SubNewTraj= new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ((CenterActivity) getActivity()).goSubNewTraj();

        }
    };


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}