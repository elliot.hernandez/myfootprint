package com.example.myfootprint.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.myfootprint.CenterActivity;
import com.example.myfootprint.R;
import com.example.myfootprint.databinding.MesTrajetsFragmentBinding;

public class MesTrajetsFragment extends Fragment {

    private MesTrajetsViewModel mViewModel;
    private Object MesTrajetsViewModel;
    private MesTrajetsFragmentBinding binding;
    private LinearLayout l1;

    public static MesTrajetsFragment newInstance() {
        return new MesTrajetsFragment();
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        MesTrajetsViewModel =
                new ViewModelProvider(this).get(MesTrajetsViewModel.class);

        binding = MesTrajetsFragmentBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        l1=root.findViewById(R.id.llmesTrajets);


        TextView lequel= new TextView(getActivity());
        String n = "<b><font size =\\\"20\\\">Voici vos trajets enregistrés:</font></b>";
        //peut aussi textView.setTextSize(X) avec X interprete en sp
        //ou setTextSize(TypedValue.COMPLEX_UNIT_DIP, X) avec X en dp
        lequel.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
        lequel.setText(Html.fromHtml(n));
        l1.addView(lequel);

        TextView tv16= new TextView(getActivity());
        tv16.setText(" ");
        l1.addView(tv16);


        //on affiche autant de boutons qu'il y a de trajets préenregistrés
        SharedPreferences sp=root.getContext().getSharedPreferences("TrajetsPreenregistres", Context.MODE_PRIVATE);
        String tsTrajets = sp.getString("tsTrajets", "");

        String[] listechar = tsTrajets.split(" ");
        if (tsTrajets=="") {
            Toast.makeText(getContext(), "Il n'y a aucun trajet préenregistré...", Toast.LENGTH_SHORT).show();
            //On n'affiche aucun bouton: pas de trajet préenregistré
        }
        else {
            for (int q = 0; q < listechar.length; q++) {
                Button but = new Button(getActivity());
                TextView v=new TextView(getActivity());
                but.setText(listechar[q]);
                but.setTextColor(Color.BLACK);;
                but.setOnClickListener(((CenterActivity) getActivity()).affiche);
                l1.addView(but);
                l1.addView(v);
            }
        }





        return root;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();

        try {
            ((CenterActivity) getActivity()).lbis.removeAllViews();
            l1.removeAllViews();
        } catch (Exception e){}
    }





}