package com.example.myfootprint.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.myfootprint.CenterActivity;
import com.example.myfootprint.LoginActivity;
import com.example.myfootprint.R;

import java.io.File;

public class ConfirmActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm);
    }

    public void reinitialiseLogin(View view) {
        File dir = new File(this.getFilesDir().getParent() + "/shared_prefs/");
        String[] children = dir.list();
        for (int i = 0; i < children.length; i++) {
        this.getSharedPreferences(children[i].replace(".xml", ""), Context.MODE_PRIVATE).edit().clear().commit();
            new File(dir, children[i]).delete();
        }
        Intent messageVersLoginActivity = new Intent();
        messageVersLoginActivity.setClass(this, LoginActivity.class);
        startActivity(messageVersLoginActivity);
    }

    public void revenirHome(View view) {
        Intent messageVersHomeActivity = new Intent();
        messageVersHomeActivity.setClass(this, CenterActivity.class);
        startActivity(messageVersHomeActivity);
    }
}