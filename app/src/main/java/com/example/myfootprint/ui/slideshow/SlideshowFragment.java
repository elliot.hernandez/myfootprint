package com.example.myfootprint.ui.slideshow;

import static java.lang.Double.valueOf;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.myfootprint.CenterActivity;
import com.example.myfootprint.R;
import com.example.myfootprint.databinding.FragmentHomeBinding;
import com.example.myfootprint.databinding.FragmentSlideshowBinding;
import com.example.myfootprint.ui.home.HomeFragment;
import com.example.myfootprint.ui.home.HomeViewModel;

public class SlideshowFragment extends Fragment {

    private SlideshowViewModel slideshowViewModel;
    private FragmentSlideshowBinding binding;
    private double coeffMarcheVelo = 0.0002;
    private double coeffBus = 0.070;
    private double coeffVoiture = 0.125;
    private double coeffTrainRERTram = 0.01;
    private double coeffAvion = 0.215;
    private double totalCarboneJour = 2.30;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        slideshowViewModel =
                new ViewModelProvider(this).get(SlideshowViewModel.class);

        binding = FragmentSlideshowBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        //recupere pseudo
        SharedPreferences sp= getActivity().getSharedPreferences("historique", Context.MODE_PRIVATE);
        String pseudo =sp.getString("pseudo", "nobody");
        //fin recup pseudo

        TextView tvPseudo = root.findViewById(R.id.textViewPseudo);
        tvPseudo.setText("Pseudo : " + pseudo);

        //recupere les 7 derniers jours

        String chaine = sp.getString("7derniers", "00/00/0000 00/00/0000 00/00/0000 00/00/0000 00/00/0000 00/00/0000 00/00/0000");
        String[] liste7Jours = ((CenterActivity) getActivity()).Dates7der(chaine);


        // CALCUL DU SCORE DE CHAQUE JOUR

        //JOUR 1


        String Date1 = liste7Jours[0];
        String chaine1 = sp.getString(Date1, "0 0 0 0 0");

        Float[] liste1;
        liste1 = ((CenterActivity) getActivity()).decode(chaine1);

        Float kmMarcheVelo1 = liste1[0];
        Float kmBus1 = liste1[1];
        Float kmVoiture1 = liste1[2];
        Float kmTrainRERTram1 = liste1[3];
        Float kmAvion1 = liste1[4];

        String strMarcheVelo1 = Float.toString(kmMarcheVelo1);
        String strBus1 = Float.toString(kmBus1);
        String strVoiture1 = Float.toString(kmVoiture1);
        String strTrainRERTram1 = Float.toString(kmTrainRERTram1);
        String strAvion1 = Float.toString(kmAvion1);

        //JOUR 2


        String Date2 = liste7Jours[1];
        String chaine2 = sp.getString(Date2, "0 0 0 0 0");

        Float[] liste2;
        liste2 = ((CenterActivity) getActivity()).decode(chaine2);

        Float kmMarcheVelo2 = liste2[0];
        Float kmBus2 = liste2[1];
        Float kmVoiture2 = liste2[2];
        Float kmTrainRERTram2 = liste2[3];
        Float kmAvion2 = liste2[4];

        String strMarcheVelo2 = Float.toString(kmMarcheVelo2);
        String strBus2 = Float.toString(kmBus2);
        String strVoiture2 = Float.toString(kmVoiture2);
        String strTrainRERTram2 = Float.toString(kmTrainRERTram2);
        String strAvion2 = Float.toString(kmAvion2);


        //JOUR 3


        String Date3 = liste7Jours[2];
        String chaine3 = sp.getString(Date3, "0 0 0 0 0");

        Float[] liste3;
        liste3 = ((CenterActivity) getActivity()).decode(chaine3);

        Float kmMarcheVelo3 = liste3[0];
        Float kmBus3 = liste3[1];
        Float kmVoiture3 = liste3[2];
        Float kmTrainRERTram3 = liste3[3];
        Float kmAvion3 = liste3[4];

        String strMarcheVelo3 = Float.toString(kmMarcheVelo3);
        String strBus3 = Float.toString(kmBus3);
        String strVoiture3 = Float.toString(kmVoiture3);
        String strTrainRERTram3 = Float.toString(kmTrainRERTram3);
        String strAvion3 = Float.toString(kmAvion3);


        //JOUR 4


        String Date4 = liste7Jours[3];
        String chaine4 = sp.getString(Date4, "0 0 0 0 0");

        Float[] liste4;
        liste4 = ((CenterActivity) getActivity()).decode(chaine4);

        Float kmMarcheVelo4 = liste4[0];
        Float kmBus4 = liste4[1];
        Float kmVoiture4 = liste4[2];
        Float kmTrainRERTram4 = liste4[3];
        Float kmAvion4 = liste4[4];

        String strMarcheVelo4 = Float.toString(kmMarcheVelo4);
        String strBus4 = Float.toString(kmBus4);
        String strVoiture4 = Float.toString(kmVoiture4);
        String strTrainRERTram4 = Float.toString(kmTrainRERTram4);
        String strAvion4 = Float.toString(kmAvion4);


        //JOUR 5


        String Date5 = liste7Jours[4];
        String chaine5 = sp.getString(Date5, "0 0 0 0 0");

        Float[] liste5;
        liste5 = ((CenterActivity) getActivity()).decode(chaine5);

        Float kmMarcheVelo5 = liste5[0];
        Float kmBus5 = liste5[1];
        Float kmVoiture5 = liste5[2];
        Float kmTrainRERTram5 = liste5[3];
        Float kmAvion5 = liste5[4];

        String strMarcheVelo5 = Float.toString(kmMarcheVelo5);
        String strBus5 = Float.toString(kmBus5);
        String strVoiture5 = Float.toString(kmVoiture5);
        String strTrainRERTram5 = Float.toString(kmTrainRERTram5);
        String strAvion5 = Float.toString(kmAvion5);


        //JOUR 6


        String Date6 = liste7Jours[5];
        String chaine6 = sp.getString(Date6, "0 0 0 0 0");

        Float[] liste6;
        liste6 = ((CenterActivity) getActivity()).decode(chaine6);

        Float kmMarcheVelo6 = liste6[0];
        Float kmBus6 = liste6[1];
        Float kmVoiture6 = liste6[2];
        Float kmTrainRERTram6 = liste6[3];
        Float kmAvion6 = liste6[4];

        String strMarcheVelo6 = Float.toString(kmMarcheVelo6);
        String strBus6 = Float.toString(kmBus6);
        String strVoiture6 = Float.toString(kmVoiture6);
        String strTrainRERTram6 = Float.toString(kmTrainRERTram6);
        String strAvion6 = Float.toString(kmAvion6);


        //JOUR 7


        String Date7 = liste7Jours[6];
        String chaine7 = sp.getString(Date7, "0 0 0 0 0");

        Float[] liste7;
        liste7 = ((CenterActivity) getActivity()).decode(chaine7);

        Float kmMarcheVelo7 = liste7[0];
        Float kmBus7 = liste7[1];
        Float kmVoiture7 = liste7[2];
        Float kmTrainRERTram7 = liste7[3];
        Float kmAvion7 = liste7[4];

        String strMarcheVelo7 = Float.toString(kmMarcheVelo7);
        String strBus7 = Float.toString(kmBus7);
        String strVoiture7 = Float.toString(kmVoiture7);
        String strTrainRERTram7 = Float.toString(kmTrainRERTram7);
        String strAvion7 = Float.toString(kmAvion7);


        /// AFFICHAGE DES SCORES (SETTEXT DANS LES TEXTVIEW) + DE LA PROGRESS BAR

        //JOUR 1
        TextView tvScore1 = root.findViewById(R.id.textViewScore1);
        TextView tvDate1 = root.findViewById(R.id.textViewDate1);
        ProgressBar progressBar1 = root.findViewById(R.id.progressBar1);

        double score1 = ((CenterActivity) getActivity()).calculScoreTotal(Date1)[0];
        double quotient1 = (score1/totalCarboneJour);
        double  doublePercent1 = quotient1*100;
        int percent1 = (int) doublePercent1;
        ((CenterActivity) getActivity()).setCouleurProgressBar(progressBar1,percent1);

        tvScore1.setText("Budget carbone consommé : " + score1 + " kgCO2");
        tvDate1.setText("Date : " + Date1);

        TextView tvMarcheVeloKM1 = root.findViewById(R.id.textViewMarcheVeloKM1);
        TextView tvMarcheVeloPTS1 = root.findViewById(R.id.textViewMarcheVeloPTS1);

        TextView tvBusKM1 = root.findViewById(R.id.textViewBusKM1);
        TextView tvBusPTS1 = root.findViewById(R.id.textViewBusPTS1);

        TextView tvVoitureKM1 = root.findViewById(R.id.textViewVoitureKM1);
        TextView tvVoiturePTS1 = root.findViewById(R.id.textViewVoiturePTS1);

        TextView tvTrainRERTramKM1 = root.findViewById(R.id.textViewTrainRERTramKM1);
        TextView tvTrainRERTramPTS1 = root.findViewById(R.id.textViewTrainRERTramPTS1);

        TextView tvAvionKM1 = root.findViewById(R.id.textViewAvionKM1);
        TextView tvAvionPTS1 = root.findViewById(R.id.textViewAvionPTS1);

        tvMarcheVeloKM1.setText(strMarcheVelo1 + " km");
        tvMarcheVeloPTS1.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmMarcheVelo1, coeffMarcheVelo)) + " kgCO2");

        tvBusKM1.setText(strBus1 + " km");
        tvBusPTS1.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmBus1, coeffBus)) + " kgCO2");

        tvVoitureKM1.setText(strVoiture1 + " km");
        tvVoiturePTS1.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmVoiture1, coeffVoiture)) + " kgCO2");

        tvTrainRERTramKM1.setText(strTrainRERTram1 + " km");
        tvTrainRERTramPTS1.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmTrainRERTram1, coeffTrainRERTram)) + " kgCO2");

        tvAvionKM1.setText(strAvion1 + " km");
        tvAvionPTS1.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmAvion1, coeffAvion)) + " kgCO2");

        //JOUR 2
        TextView tvScore2 = root.findViewById(R.id.textViewScore2);
        TextView tvDate2 = root.findViewById(R.id.textViewDate2);
        ProgressBar progressBar2 = root.findViewById(R.id.progressBar2);

        double score2 = ((CenterActivity) getActivity()).calculScoreTotal(Date2)[0];
        double quotient2 = (score2/totalCarboneJour);
        double  doublePercent2 = quotient2*100;
        int percent2 = (int) doublePercent2;
        ((CenterActivity) getActivity()).setCouleurProgressBar(progressBar2,percent2);
        tvScore2.setText("Budget carbone consommé : " + score2 + " kgCO2");
        tvDate2.setText("Date : " + Date2);

        TextView tvMarcheVeloKM2 = root.findViewById(R.id.textViewMarcheVeloKM2);
        TextView tvMarcheVeloPTS2 = root.findViewById(R.id.textViewMarcheVeloPTS2);

        TextView tvBusKM2 = root.findViewById(R.id.textViewBusKM2);
        TextView tvBusPTS2 = root.findViewById(R.id.textViewBusPTS2);

        TextView tvVoitureKM2 = root.findViewById(R.id.textViewVoitureKM2);
        TextView tvVoiturePTS2 = root.findViewById(R.id.textViewVoiturePTS2);

        TextView tvTrainRERTramKM2 = root.findViewById(R.id.textViewTrainRERTramKM2);
        TextView tvTrainRERTramPTS2 = root.findViewById(R.id.textViewTrainRERTramPTS2);

        TextView tvAvionKM2 = root.findViewById(R.id.textViewAvionKM2);
        TextView tvAvionPTS2 = root.findViewById(R.id.textViewAvionPTS2);

        tvMarcheVeloKM2.setText(strMarcheVelo2 + " km");
        tvMarcheVeloPTS2.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmMarcheVelo2, coeffMarcheVelo)) + " kgCO2");

        tvBusKM2.setText(strBus2 + " km");
        tvBusPTS2.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmBus2, coeffBus)) + " kgCO2");

        tvVoitureKM2.setText(strVoiture2 + " km");
        tvVoiturePTS2.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmVoiture2, coeffVoiture)) + " kgCO2");

        tvTrainRERTramKM2.setText(strTrainRERTram2 + " km");
        tvTrainRERTramPTS2.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmTrainRERTram2, coeffTrainRERTram)) + " kgCO2");

        tvAvionKM2.setText(strAvion2 + " km");
        tvAvionPTS2.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmAvion2, coeffAvion)) + " kgCO2");

        //JOUR 3
        TextView tvScore3 = root.findViewById(R.id.textViewScore3);
        TextView tvDate3 = root.findViewById(R.id.textViewDate3);
        ProgressBar progressBar3 = root.findViewById(R.id.progressBar3);

        double score3 = ((CenterActivity) getActivity()).calculScoreTotal(Date3)[0];
        double quotient3 = (score3/totalCarboneJour);
        double  doublePercent3 = quotient3*100;
        int percent3 = (int) doublePercent3;
        ((CenterActivity) getActivity()).setCouleurProgressBar(progressBar3,percent3);
        tvScore3.setText("Budget carbone consommé : " + score3 + " kgCO2");
        tvDate3.setText("Date : " + Date3);

        TextView tvMarcheVeloKM3 = root.findViewById(R.id.textViewMarcheVeloKM3);
        TextView tvMarcheVeloPTS3 = root.findViewById(R.id.textViewMarcheVeloPTS3);

        TextView tvBusKM3 = root.findViewById(R.id.textViewBusKM3);
        TextView tvBusPTS3 = root.findViewById(R.id.textViewBusPTS3);

        TextView tvVoitureKM3 = root.findViewById(R.id.textViewVoitureKM3);
        TextView tvVoiturePTS3 = root.findViewById(R.id.textViewVoiturePTS3);

        TextView tvTrainRERTramKM3 = root.findViewById(R.id.textViewTrainRERTramKM3);
        TextView tvTrainRERTramPTS3 = root.findViewById(R.id.textViewTrainRERTramPTS3);

        TextView tvAvionKM3 = root.findViewById(R.id.textViewAvionKM3);
        TextView tvAvionPTS3 = root.findViewById(R.id.textViewAvionPTS3);

        tvMarcheVeloKM3.setText(strMarcheVelo3 + " km");
        tvMarcheVeloPTS3.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmMarcheVelo3, coeffMarcheVelo)) + " kgCO2");

        tvBusKM3.setText(strBus3 + " km");
        tvBusPTS3.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmBus3, coeffBus)) + " kgCO2");

        tvVoitureKM3.setText(strVoiture3 + " km");
        tvVoiturePTS3.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmVoiture3, coeffVoiture)) + " kgCO2");

        tvTrainRERTramKM3.setText(strTrainRERTram3 + " km");
        tvTrainRERTramPTS3.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmTrainRERTram3, coeffTrainRERTram)) + " kgCO2");

        tvAvionKM3.setText(strAvion3 + " km");
        tvAvionPTS3.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmAvion3, coeffAvion)) + " kgCO2");

        //JOUR 4
        TextView tvScore4 = root.findViewById(R.id.textViewScore4);
        TextView tvDate4 = root.findViewById(R.id.textViewDate4);
        ProgressBar progressBar4 = root.findViewById(R.id.progressBar4);

        double score4 = ((CenterActivity) getActivity()).calculScoreTotal(Date4)[0];
        double quotient4 = (score4/totalCarboneJour);
        double  doublePercent4 = quotient4*100;
        int percent4 = (int) doublePercent4;
        ((CenterActivity) getActivity()).setCouleurProgressBar(progressBar4,percent4);
        tvScore4.setText("Budget carbone consommé : " + score4 + " kgCO2");
        tvDate4.setText("Date : " + Date4);

        TextView tvMarcheVeloKM4 = root.findViewById(R.id.textViewMarcheVeloKM4);
        TextView tvMarcheVeloPTS4 = root.findViewById(R.id.textViewMarcheVeloPTS4);

        TextView tvBusKM4 = root.findViewById(R.id.textViewBusKM4);
        TextView tvBusPTS4 = root.findViewById(R.id.textViewBusPTS4);

        TextView tvVoitureKM4 = root.findViewById(R.id.textViewVoitureKM4);
        TextView tvVoiturePTS4 = root.findViewById(R.id.textViewVoiturePTS4);

        TextView tvTrainRERTramKM4 = root.findViewById(R.id.textViewTrainRERTramKM4);
        TextView tvTrainRERTramPTS4 = root.findViewById(R.id.textViewTrainRERTramPTS4);

        TextView tvAvionKM4 = root.findViewById(R.id.textViewAvionKM4);
        TextView tvAvionPTS4 = root.findViewById(R.id.textViewAvionPTS4);

        tvMarcheVeloKM4.setText(strMarcheVelo4 + " km");
        tvMarcheVeloPTS4.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmMarcheVelo4, coeffMarcheVelo)) + " kgCO2");

        tvBusKM4.setText(strBus4 + " km");
        tvBusPTS4.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmBus4, coeffBus)) + " kgCO2");

        tvVoitureKM4.setText(strVoiture4 + " km");
        tvVoiturePTS4.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmVoiture4, coeffVoiture)) + " kgCO2");

        tvTrainRERTramKM4.setText(strTrainRERTram4 + " km");
        tvTrainRERTramPTS4.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmTrainRERTram4, coeffTrainRERTram)) + " kgCO2");

        tvAvionKM4.setText(strAvion4 + " km");
        tvAvionPTS4.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmAvion4, coeffAvion)) + " kgCO2");

        //JOUR 5
        TextView tvScore5 = root.findViewById(R.id.textViewScore5);
        TextView tvDate5 = root.findViewById(R.id.textViewDate5);
        ProgressBar progressBar5 = root.findViewById(R.id.progressBar5);

        double score5 = ((CenterActivity) getActivity()).calculScoreTotal(Date5)[0];
        double quotient5 = (score5/totalCarboneJour);
        double  doublePercent5 = quotient5*100;
        int percent5 = (int) doublePercent5;
        ((CenterActivity) getActivity()).setCouleurProgressBar(progressBar5,percent5);
        tvScore5.setText("Budget carbone consommé : " + score5 + " kgCO2");
        tvDate5.setText("Date : " + Date5);

        TextView tvMarcheVeloKM5 = root.findViewById(R.id.textViewMarcheVeloKM5);
        TextView tvMarcheVeloPTS5 = root.findViewById(R.id.textViewMarcheVeloPTS5);

        TextView tvBusKM5 = root.findViewById(R.id.textViewBusKM5);
        TextView tvBusPTS5 = root.findViewById(R.id.textViewBusPTS5);

        TextView tvVoitureKM5 = root.findViewById(R.id.textViewVoitureKM5);
        TextView tvVoiturePTS5 = root.findViewById(R.id.textViewVoiturePTS5);

        TextView tvTrainRERTramKM5 = root.findViewById(R.id.textViewTrainRERTramKM5);
        TextView tvTrainRERTramPTS5 = root.findViewById(R.id.textViewTrainRERTramPTS5);

        TextView tvAvionKM5 = root.findViewById(R.id.textViewAvionKM5);
        TextView tvAvionPTS5 = root.findViewById(R.id.textViewAvionPTS5);

        tvMarcheVeloKM5.setText(strMarcheVelo5 + " km");
        tvMarcheVeloPTS5.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmMarcheVelo5, coeffMarcheVelo)) + " kgCO2");

        tvBusKM5.setText(strBus5 + " km");
        tvBusPTS5.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmBus5, coeffBus)) + " kgCO2");

        tvVoitureKM5.setText(strVoiture5 + " km");
        tvVoiturePTS5.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmVoiture5, coeffVoiture)) + " kgCO2");

        tvTrainRERTramKM5.setText(strTrainRERTram5 + " km");
        tvTrainRERTramPTS5.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmTrainRERTram5, coeffTrainRERTram)) + " kgCO2");

        tvAvionKM5.setText(strAvion5 + " km");
        tvAvionPTS5.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmAvion5, coeffAvion)) + " kgCO2");

        //JOUR 6
        TextView tvScore6 = root.findViewById(R.id.textViewScore6);
        TextView tvDate6 = root.findViewById(R.id.textViewDate6);
        ProgressBar progressBar6 = root.findViewById(R.id.progressBar6);

        double score6 = ((CenterActivity) getActivity()).calculScoreTotal(Date6)[0];
        double quotient6 = (score6/totalCarboneJour);
        double  doublePercent6 = quotient6*100;
        int percent6 = (int) doublePercent6;
        ((CenterActivity) getActivity()).setCouleurProgressBar(progressBar6,percent6);
        tvScore6.setText("Budget carbone consommé : " + score6 + " kgCO2");
        tvDate6.setText("Date : " + Date6);

        TextView tvMarcheVeloKM6 = root.findViewById(R.id.textViewMarcheVeloKM6);
        TextView tvMarcheVeloPTS6 = root.findViewById(R.id.textViewMarcheVeloPTS6);

        TextView tvBusKM6 = root.findViewById(R.id.textViewBusKM6);
        TextView tvBusPTS6 = root.findViewById(R.id.textViewBusPTS6);

        TextView tvVoitureKM6 = root.findViewById(R.id.textViewVoitureKM6);
        TextView tvVoiturePTS6 = root.findViewById(R.id.textViewVoiturePTS6);

        TextView tvTrainRERTramKM6 = root.findViewById(R.id.textViewTrainRERTramKM6);
        TextView tvTrainRERTramPTS6 = root.findViewById(R.id.textViewTrainRERTramPTS6);

        TextView tvAvionKM6 = root.findViewById(R.id.textViewAvionKM6);
        TextView tvAvionPTS6 = root.findViewById(R.id.textViewAvionPTS6);

        tvMarcheVeloKM6.setText(strMarcheVelo6 + " km");
        tvMarcheVeloPTS6.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmMarcheVelo6, coeffMarcheVelo)) + " kgCO2");

        tvBusKM6.setText(strBus6 + " km");
        tvBusPTS6.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmBus6, coeffBus)) + " kgCO2");

        tvVoitureKM6.setText(strVoiture6 + " km");
        tvVoiturePTS6.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmVoiture6, coeffVoiture)) + " kgCO2");

        tvTrainRERTramKM6.setText(strTrainRERTram6 + " km");
        tvTrainRERTramPTS6.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmTrainRERTram6, coeffTrainRERTram)) + " kgCO2");

        tvAvionKM6.setText(strAvion6 + " km");
        tvAvionPTS6.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmAvion6, coeffAvion)) + " kgCO2");

        //JOUR 7
        TextView tvScore7 = root.findViewById(R.id.textViewScore7);
        TextView tvDate7 = root.findViewById(R.id.textViewDate7);
        ProgressBar progressBar7 = root.findViewById(R.id.progressBar7);

        double score7 = ((CenterActivity) getActivity()).calculScoreTotal(Date7)[0];
        double quotient7 = (score7/totalCarboneJour);
        double  doublePercent7 = quotient7*100;
        int percent7 = (int) doublePercent7;
        ((CenterActivity) getActivity()).setCouleurProgressBar(progressBar7,percent7);
        tvScore7.setText("Budget carbone consommé : " + score7 + " kgCO2");
        tvDate7.setText("Date : " + Date7);

        TextView tvMarcheVeloKM7 = root.findViewById(R.id.textViewMarcheVeloKM7);
        TextView tvMarcheVeloPTS7 = root.findViewById(R.id.textViewMarcheVeloPTS7);

        TextView tvBusKM7 = root.findViewById(R.id.textViewBusKM7);
        TextView tvBusPTS7 = root.findViewById(R.id.textViewBusPTS7);

        TextView tvVoitureKM7 = root.findViewById(R.id.textViewVoitureKM7);
        TextView tvVoiturePTS7 = root.findViewById(R.id.textViewVoiturePTS7);

        TextView tvTrainRERTramKM7 = root.findViewById(R.id.textViewTrainRERTramKM7);
        TextView tvTrainRERTramPTS7 = root.findViewById(R.id.textViewTrainRERTramPTS7);

        TextView tvAvionKM7 = root.findViewById(R.id.textViewAvionKM7);
        TextView tvAvionPTS7 = root.findViewById(R.id.textViewAvionPTS7);

        tvMarcheVeloKM7.setText(strMarcheVelo7 + " km");
        tvMarcheVeloPTS7.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmMarcheVelo7, coeffMarcheVelo)) + " kgCO2");

        tvBusKM7.setText(strBus7 + " km");
        tvBusPTS7.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmBus7, coeffBus)) + " kgCO2");

        tvVoitureKM7.setText(strVoiture7 + " km");
        tvVoiturePTS7.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmVoiture7, coeffVoiture)) + " kgCO2");

        tvTrainRERTramKM7.setText(strTrainRERTram7 + " km");
        tvTrainRERTramPTS7.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmTrainRERTram7, coeffTrainRERTram)) + " kgCO2");

        tvAvionKM7.setText(strAvion7 + " km");
        tvAvionPTS7.setText(valueOf(((CenterActivity) getActivity()).calculScore(kmAvion7, coeffAvion)) + " kgCO2");

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}